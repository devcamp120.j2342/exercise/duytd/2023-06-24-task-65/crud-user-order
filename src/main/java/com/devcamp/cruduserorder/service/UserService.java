package com.devcamp.cruduserorder.service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.devcamp.cruduserorder.model.COrder;
import com.devcamp.cruduserorder.model.CUser;
import com.devcamp.cruduserorder.repository.OrderRepository;
import com.devcamp.cruduserorder.repository.UserRepository;

@Service
public class UserService {
    @Autowired
    UserRepository userRepository;

    @Autowired
    OrderRepository orderRepository;

    public List<CUser> getAllUserSV() {
        List<CUser> userList = userRepository.findAll();
        return userList;
    }

    public ResponseEntity<Object> getUserByIdSV(long id) {
        try {
            Optional <CUser> optionalUser = userRepository.findById(id);
            if(optionalUser.isPresent()) {
                return new ResponseEntity<>(optionalUser.get(), HttpStatus.OK);
            }else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        }catch(Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public CUser createUserSV( CUser cUserCilent) {
            CUser user = new CUser();
            user.setAddress(cUserCilent.getAddress());
            user.setEmail(cUserCilent.getEmail());
            user.setFullname(cUserCilent.getFullname());
            user.setPhone(cUserCilent.getPhone());
            user.setCreated(LocalDateTime.now());
            user.setUpdated(null);
            CUser saveUser = userRepository.save(user);
            return saveUser;
    }

    public CUser updateUserSV(long id, CUser cUserUpdate) {
        Optional<CUser> optionalUser = userRepository.findById(id);
            if(optionalUser.isPresent()) {
                CUser user = optionalUser.get();
                user.setFullname(cUserUpdate.getFullname());
                user.setAddress(cUserUpdate.getAddress());
                user.setEmail(cUserUpdate.getEmail());
                user.setPhone(cUserUpdate.getPhone());
                CUser saveUser = userRepository.save(user);
                return saveUser;
            }else {
                return null;
            }
    }

    public CUser deleteUser(long id) {
        if(userRepository.findById(id).isPresent()) {
            userRepository.deleteById(id);
            return null;
        }else {
            return null;
        }
    }

    public List<COrder> getOrderByUser(Long id) {
        return orderRepository.getOrderBycUserId(id);
    }

}
