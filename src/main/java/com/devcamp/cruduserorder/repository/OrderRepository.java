package com.devcamp.cruduserorder.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.cruduserorder.model.COrder;

public interface OrderRepository extends JpaRepository<COrder, Long>{
   List<COrder> getOrderBycUserId(Long id);
}
