package com.devcamp.cruduserorder.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.cruduserorder.model.CUser;

public interface UserRepository extends JpaRepository<CUser, Long>{
}
