package com.devcamp.cruduserorder.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.cruduserorder.model.CUser;
import com.devcamp.cruduserorder.repository.UserRepository;
import com.devcamp.cruduserorder.service.UserService;

@CrossOrigin
@RestController
@RequestMapping("/")
public class CUserController {
    @Autowired
    private UserService userService;

    @Autowired
    private UserRepository userRepository;

    @GetMapping("/users")
    public List<CUser> getAllUser() {
        return userService.getAllUserSV();
    }

    @GetMapping("/user/{id}")
    public ResponseEntity<Object> getUserById(@PathVariable(value="id") long id) {
        return userService.getUserByIdSV(id);
    }

    @PostMapping("/users")
    public ResponseEntity<Object> createdUser(@RequestBody CUser cUserCilent) {
        try {
            return new ResponseEntity<>(userService.createUserSV(cUserCilent), HttpStatus.CREATED);
        }catch(Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/users/{id}")
    public ResponseEntity<Object> updateUser(@PathVariable(value = "id") long id, @RequestBody CUser userUpdate) {
        try{
            return new ResponseEntity<>(userService.updateUserSV(id, userUpdate), HttpStatus.OK);
        }catch(Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("users/{id}")
    public ResponseEntity<Object> deleteUser(@PathVariable(value="id") long id) {
        try {
            return new ResponseEntity<>(userService.deleteUser(id), HttpStatus.NO_CONTENT);
        }catch(Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/users/count")
    public Long countUser() {
        return userRepository.count();
    }

    @GetMapping("/users/check/{id}")
    public Boolean checkUser(@PathVariable(value="id") long id) {
        if(userRepository.findById(id).isPresent()){
            return userRepository.existsById(id);
        }else {
            return false;
        }
    }

    @GetMapping("/users/page")
    public ResponseEntity<Object> getUserPage(
        @RequestParam(value="page", defaultValue = "1") String page,
        @RequestParam(value="size", defaultValue = "5") String size) {
            try {
                Pageable pageElement = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size));
                List<CUser> list = new ArrayList<CUser>();
                userRepository.findAll(pageElement).forEach(list::add);
                return new ResponseEntity<>(list, HttpStatus.OK);
            }catch(Exception e) {
                return null;
            }
            

        }
}
