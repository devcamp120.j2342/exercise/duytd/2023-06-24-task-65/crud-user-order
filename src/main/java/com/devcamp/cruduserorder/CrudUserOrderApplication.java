package com.devcamp.cruduserorder;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CrudUserOrderApplication {

	public static void main(String[] args) {
		SpringApplication.run(CrudUserOrderApplication.class, args);
	}

}
